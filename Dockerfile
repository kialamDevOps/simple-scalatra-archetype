from tomcat
maintainer [you]
copy target/simple-scalatra-archetype-2.5-SNAPSHOT.jar /usr/local/tomcat/webapps/scalatra-maven-prototype.war
expose 8080
CMD ["catalina.sh", "run"]